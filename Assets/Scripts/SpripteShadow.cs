using UnityEngine;

public class SpripteShadow : MonoBehaviour
{

    void Start()
    {
        GetComponent<SpriteRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
    }

}
