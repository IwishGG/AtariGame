using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteamingPipe : MonoBehaviour
{
    [SerializeField]
    private float _steamPowerMax = 30f;
    [SerializeField]
    private float _steamPowerMin = 15f;
    [SerializeField]
    private ParticleSystem _steam;
    [SerializeField]
    private float _steamDurationTime;
    [SerializeField]
    private float _steamStopTime;
    [SerializeField]
    private ForceMode _forceMode = ForceMode.Force;

    private float _timer1;
    private float _timer2;
    private float _steamPower;



    private void Start()
    {
        _steam.Play();
        _timer1 = _steamDurationTime;
        _timer2 = _steamStopTime;


    }
    private void Update()
    {
        SteamTimer();
        _steamPower = Random.Range(_steamPowerMin, _steamPowerMax);
    }

    private void OnTriggerStay(Collider other)
    {
        Rigidbody _rb = other.GetComponent<Rigidbody>();
        if (other.transform.GetComponent<MovementController>() || other.transform.GetComponent<Ball>())
        {
             _rb.AddForce(this.transform.forward * _steamPower, _forceMode);
            Debug.Log("Steam");
        }
    }
    private void SteamTimer()
    {
        if (_steamDurationTime > 0)
        {

            _steamDurationTime -= Time.deltaTime;
            gameObject.GetComponent<BoxCollider>().enabled = true;
            _steam.Play();

        }
        if (_steamDurationTime <= 0)
        {
            if (_steamStopTime > 0)
            {
                _steamStopTime -= Time.deltaTime;
                gameObject.GetComponent<BoxCollider>().enabled = false;
                _steam.Stop();
            }
            if (_steamStopTime <= 0)
            {
                _steamDurationTime = _timer1;
                _steamStopTime = _timer2;

            }
        }



    }


}
