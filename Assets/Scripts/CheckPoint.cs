using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    public static GameManager Gm;

    [SerializeField]
    private AudioSource _sound;
    [SerializeField]
    private GameObject _fireFX;

    [SerializeField]
    private Animator _animator;

    private Vector3 _pos;
    private void Start()
    {
        Gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
        _pos = this.transform.position;
        _fireFX.gameObject.SetActive(false);
        _animator.SetBool("CheckPointAnim", false);
    }
    private void Awake()
    {
        _animator.SetBool("CheckPointAnim", false);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Ball>())
        {
            if (GameManager.CanControlPlayer)
            {
                _fireFX.gameObject.SetActive(true);
                _animator.SetBool("CheckPointAnim", true);
                if (Gm.lastCheckPointPos != _pos)
                {
                    Gm.lastCheckPointPos = transform.position;
                    _sound.PlayDelayed(0.2f);
                    Debug.Log("Checked");

                }

            }

        }
    }
}
