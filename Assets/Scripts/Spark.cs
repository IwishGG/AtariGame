using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Spark : MonoBehaviour
{
    [SerializeField]
    private AudioSource _spark;

    void OnCollisionEnter(Collision other)
    {
        var colRb = other.gameObject.GetComponent<Rigidbody>();

        if (other.gameObject.GetComponent<MovementController>())
        {
            Debug.Log("spark");
            if (_spark.isPlaying == false)
            {
                _spark.Play();
            }
            GameManager.CanControlPlayer = false;
            colRb.constraints = RigidbodyConstraints.None;
            StartCoroutine(LoadScene(2f));
        }
    }
    IEnumerator LoadScene(float time)
    {
        yield return new WaitForSeconds(time);
        if (GameManager.GameWin == false)
        {
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }
    }
}
