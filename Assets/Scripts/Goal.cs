using UnityEngine;

public class Goal : MonoBehaviour
{
    [SerializeField]
    private AudioSource _winSound;
    [SerializeField]
    private Animator _animator;
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Ball>())
        {
            if (GameManager.CanControlPlayer)
            {
                GameManager.CanControlPlayer = false;
                GameManager.GameWin = true;
                _winSound.Play();
                _animator.SetBool("isGoal", true);
            }
        }
    }
}
