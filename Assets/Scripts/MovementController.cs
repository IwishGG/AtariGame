using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Rigidbody))]
public class MovementController : MonoBehaviour
{

    [SerializeField]
    private Animator _animator;

    [SerializeField]
    private float _attackDistanceMins = 0f;

    [SerializeField]
    private float _moveSpeed = 20f;
    [SerializeField]
    private float _dsahSpeed = 20f;

    [SerializeField]
    private float _acceleration = 5f;

    [SerializeField]
    private LayerMask _groundLayer;

    [SerializeField]
    private float _castOffset = 0.25f;

    [SerializeField]
    private ForceMode _forceMode = ForceMode.Acceleration;

    [SerializeField]
    private AudioSource _dashSound;
    [SerializeField]
    private ParticleSystem _dashFX;
    [SerializeField]
    private Ball _ball;


    private Rigidbody _rigidBody;
    private Vector2 _moveInput;
    private Vector3 _dashDir;
    private bool _isDash;
    private bool _isCharging;
    private float _increaseSpeed = 2f;
    private Controls _input;



    private void Awake()
    {

        _rigidBody = GetComponent<Rigidbody>();
        _rigidBody.velocity = new Vector3(0, 0, 0);
        _moveInput = new Vector2(0, 0);

        CheckPoint.Gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
        transform.position = CheckPoint.Gm.lastCheckPointPos;

        _input = new Controls();
        _input.Player.MoveBoard.performed += ctx => _moveInput = _input.Player.MoveBoard.ReadValue<Vector2>();
        _input.Player.MoveBoard.canceled += ctx => _moveInput = _input.Player.MoveBoard.ReadValue<Vector2>();
    }

    // You would replace this with Move(Vector2 moveVector) when using the new InputSystem
    private void Update()
    {
        if (GameManager.CanControlPlayer)
        {
            Updateanimation();
        }
        DistanceCheck();
    }

    private void DistanceCheck()
    {
        if (_rigidBody.velocity.sqrMagnitude > 40f)
        {
            _dashFX.Emit(1);
        }
        else
        {
            _dashFX.Stop();
        }

        float distanceX = Mathf.Abs(transform.position.x - _ball.transform.position.x);
        float distanceY = Mathf.Abs(transform.position.y - _ball.transform.position.y);
        float distanceZ = Mathf.Abs(transform.position.z - _ball.transform.position.z);
        if (distanceX > 80f || distanceY > 80f || distanceZ > 80f)
        {
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }
    }

    private void Updateanimation()
    {
        _animator.SetFloat("Axis_X", _moveInput.x);
        _animator.SetFloat("Axis_Y", _moveInput.y);
    }

    private void FixedUpdate()
    {

        var raycastPos = transform.position;
        raycastPos += transform.forward * _castOffset;
        raycastPos += Vector3.up * _castOffset;
        Ray downRaw = new Ray(raycastPos, Vector3.down);

        if (Physics.Raycast(downRaw, out var hit, Mathf.Infinity, _groundLayer) && GameManager.CanControlPlayer)
        {
            var groundNormal = hit.normal;
            var right = this.transform.right;
            var forward = Vector3.Cross(-groundNormal, right);
            Debug.DrawRay(transform.position, forward, Color.red);


            var currentVel = _rigidBody.velocity;
            var targetVel = (forward * _moveInput.y + right * _moveInput.x).normalized * _moveSpeed;
            _dashDir = (forward * _moveInput.y + right * _moveInput.x).normalized;
            targetVel.y = currentVel.y;
            var force = (targetVel - currentVel) * _acceleration;
            _rigidBody.AddForce(force, _forceMode);
        }
        ApplyDash();



    }

    public void Move(InputAction.CallbackContext context)
    {
        _moveInput = context.ReadValue<Vector2>();


    }

    public void Dash(InputAction.CallbackContext context)
    {
        if (context.started && GameManager.CanControlPlayer)
        {
            _isDash = true;

        }

    }
    public void Attack(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            _isCharging = true;

        }
        if (context.canceled)
        {
            _isCharging = false;

        }

    }

    private void ApplyDash()
    {
        Vector3 force = new Vector3(0, 0, 0);
        var currentVel = _rigidBody.velocity;
        var targetVel = _dashDir * _dsahSpeed;
        if (_isDash == true)
        {
            force = (targetVel - currentVel) * _acceleration;
            _dashSound.Play();
            _dashFX.Emit(1);
            _isDash = false;
        }
        _rigidBody.AddForce(force, _forceMode);

    }

    private void Charging()
    {
        _attackDistanceMins = Mathf.Clamp(_attackDistanceMins, 0f, 5f);
        if (_isCharging == true)
        {
            _attackDistanceMins += _increaseSpeed * Time.deltaTime;

        }
        if (_isCharging == false)
        {
            _attackDistanceMins -= _increaseSpeed * Time.deltaTime;
        }
    }
    void OnEnable()
    {
        _input.Player.Enable();
    }
    void OnDisable()
    {
        _input.Player.Disable();

    }


}