using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeadZone : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        var colRb = other.gameObject.GetComponent<Rigidbody>();

        if (other.gameObject.GetComponent<Ball>() || other.gameObject.GetComponent<MovementController>())
        {
            if (GameManager.CanControlPlayer)
            {
                GameManager.CanControlPlayer = false;
                colRb.constraints = RigidbodyConstraints.None;
                StartCoroutine(LoadScene(1f));

            }

        }
    }
    IEnumerator LoadScene(float time)
    {
        yield return new WaitForSeconds(time);
        if (GameManager.GameWin == false)
        {
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }
    }
}
