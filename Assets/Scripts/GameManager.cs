using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static bool CanControlPlayer;
    public static bool GameWin;
    public static bool IsPaused;



    private static GameManager instance;
    public Vector3 lastCheckPointPos;


    private MovementController _player;

    private Ball _ball;

    void Awake()
    {
       
        CanControlPlayer = true;
        GameWin = false;
        IsPaused = false;

        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Update()
    {
        

    }
}
