using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Boom : MonoBehaviour
{
    private Collider[] _inExplosionRadius = null;
    [SerializeField]
    private float _forceMuilt = 5f;
    [SerializeField]
    private float _radius = 5f;
    [SerializeField]
    private AudioSource _boom;
    [SerializeField]
    private GameObject _explosion;
    [SerializeField]
    private ForceMode _forceMode = ForceMode.Force;

    void OnCollisionEnter(Collision other)
    {
        if (other.collider.GetComponent<Ball>() || other.collider.GetComponent<MovementController>())
        {
            StartCoroutine(LoadScene(3.5f));
            Explode();

            //_light.SetActive(false);

        }
    }

    private void Explode()
    {

        _inExplosionRadius = Physics.OverlapSphere(transform.position, _radius);

        foreach (Collider item in _inExplosionRadius)
        {
            Rigidbody itemRb = item.GetComponent<Rigidbody>();
            if (itemRb != null)
            {
                Vector2 distance = (item.transform.position - transform.position);
                if (distance.magnitude > 0)
                {
                    GameManager.CanControlPlayer = false;
                    Debug.Log("Boom");

                    float explosionForce = _forceMuilt / distance.magnitude;
                    itemRb.AddForce((distance.normalized) * explosionForce, _forceMode);
                    itemRb.constraints = RigidbodyConstraints.None;
                    if (_boom.isPlaying == false)
                    {
                        _boom.Play();
                        Instantiate(_explosion, gameObject.transform.position, gameObject.transform.rotation);
                    }

                }
            }
        }
    }

    IEnumerator LoadScene(float time)
    {
        yield return new WaitForSeconds(time);
        GameManager.CanControlPlayer = true;

        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);

    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, _radius);
    }
}
