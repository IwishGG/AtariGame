using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class Ball : MonoBehaviour
{
    [SerializeField]
    private AudioSource _roolling;

    private Rigidbody _rigidBody;
    private Collider _col​​​​​​​​​​​​​​​​​​​;
    private bool _isGround;

    [SerializeField]
    private LayerMask _groundLayer;

    void Awake()
    {
        _rigidBody = GetComponent<Rigidbody>();
        _col = GetComponent<Collider​​​​​​​​​​​​​​​​​​​>();
        CheckPoint.Gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
        transform.position = CheckPoint.Gm.lastCheckPointPos + new Vector3(0f, 2f, 3f);
    }
    private void Update()
    {
        GroundedCheck();
        if (_isGround && _rigidBody.velocity.sqrMagnitude > 0.1f)
        {
            if (!_roolling.isPlaying)
            {
                float r = Random.Range(1f, 2f);
                _roolling.pitch = r;
                _roolling.Play();
            }
        }
        else
        {
            if (_roolling.isPlaying)
            {
                _roolling.Pause();
            }
        }


    }

    private void GroundedCheck()
    {

        _isGround = Physics.Raycast(transform.position, Vector3.down, out var hit, 5f, _groundLayer);

        Debug.DrawRay(transform.position, Vector3.down, Color.red);
    }
}
