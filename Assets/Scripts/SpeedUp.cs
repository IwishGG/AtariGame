using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class SpeedUp : MonoBehaviour
{
    [SerializeField]
    private float _upSpeed;
    [SerializeField]
    private ForceMode _forceMode = ForceMode.Force;
    [SerializeField]
    private AudioSource _flipSound;

    private Rigidbody _rb;
    private Collider _collider;

    private void Awake()
    {
        _collider = GetComponent<Collider>();
        _collider.isTrigger = true;
    }

    void OnTriggerStay(Collider other)
    {
        _rb = other.GetComponent<Rigidbody>();

        if (other.GetComponent<MovementController>() || other.GetComponent<Ball>())
        {
            _rb.AddForce(this.transform.forward * _upSpeed, _forceMode);
            Debug.Log("speed up");
            if (!_flipSound.isPlaying)
            {
                _flipSound.Play();
            }

        }
    }



}
